*** Settings ***
Library    Selenium2Library

*** Variables ***
${URL}    https://parfums.ua/
${BROWSER}    chrome

*** Keywords ***
Launch Browser
  Open Browser    ${URL}    ${BROWSER}
  Maximize Browser Window

Login To The Shop
  [Arguments]    ${Username}    ${Password}
  Click Element    id=login-url
  Wait Until Page Contains    Вход
  Input Text    username    ${Username}
  Input Password    password    ${Password}
  Click Element    login-submit
  Wait Until Page Contains    Наталия Валеева

Check link from header
  [Arguments]    ${locator1}    ${locator2}    ${text}
  Click Element    ${locator1}
  Element Should Contain    ${locator2}    ${text}

Check blog link
  [Arguments]    ${locator1}    ${locator2}
  Click Element    ${locator1}
  Click Element    ${locator2}

Check Vkontakte link
  [Arguments]    ${locator1}    ${locator2}    ${text}
  Click Element    ${locator1}
  Select Window    url=https://vk.com/parfums_ua
  Element Should Contain    ${locator2}    ${text}

Check Facebook link
  [Arguments]    ${locator1}    ${locator2}    ${text}
  Click Element    ${locator1}
  Select Window    ${locator2}
  Wait Until Page Contains    ${text}

Check other social network link
  [Arguments]    ${locator1}    ${locator2}    ${text}
  Click Element    ${locator1}
  Element Should Contain    ${locator2}    ${text}
  Go Back
