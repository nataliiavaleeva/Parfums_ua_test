*** Settings ***
Resource  _resource.robot
Suite Setup  Launch Browser
Suite Teardown  Close All Browsers

*** Variables ***

*** Test Cases ***
User should login and confirm
  [Documentation]    User shoul login to the system and confirm it
  Wait Until Page Contains Element    css=span.va-m
  Click Element    id=login-url
  Wait Until Page Contains    Вход
  Login To The System    380976165637    380976165637
  Wait Until Page Contains    Наталия Валеева
  Click Link    css=a.logout-link

Empty fields
  [Documentation]    User tries to login with empty fields
  Click Element    id=login-url
  Wait Until Page Contains   Вход
  Click Element    login-submit
  Wait Until Element Contains     css=div#login-error    Неверные данные для входа!

Empty password
  [Documentation]    User tries to login with valid username but empty password
  Login To The System    380976165637    ${empty}
  Wait Until Element Contains     css=div#login-error    Неверные данные для входа!

Empty username
  [Documentation]    User tries to login without username and valid password
  Login To The System    ${empty}    380976165637
  Wait Until Element Contains     css=div#login-error    Неверные данные для входа!
  Click Element    css==span.close-btn

Wrong data
  [Documentation]    User tries to login with wrong data
  Click Element    id=login-url
  Wait Until Page Contains    Вход
  Login To The System    2345679    try55679
  Wait Until Element Contains     css=div#login-error    Неверные данные для входа!

*** Keywords ***
Login To The System
  [Arguments]    ${Username}    ${Password}
  Input Text    username    ${Username}
  Input Password    password    ${Password}
  Click Element    login-submit