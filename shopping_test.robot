*** Settings ***
Resource    _resource.robot
Suite Setup    Launch Browser
Suite Teardown    Close all browsers

*** Variables ***
${PRICE1}    235
${PRICE2}    21
${PRICE3}    206
${TOTPRICE1}    256
${TOTPRICE2}    277

*** Test Cases ***
User login to the shop
  Login To The Shop    380976165637    380976165637

Search goods and put them to the cart
  [Documentation]  User is looking for the goods and adding them to the cart
  Search Of Goods    tide гель
  Adding To The Cart  css=.popup-search-results>ul li:first-child a  ${PRICE1}  css=a[data-sku-priceoriginal="235"]
  Verify Cart    ${PRICE1}
  Click link    id=continue-shopping
  Search Of Goods    dove крем-мыло
  Adding To The Cart  css=.popup-search-results>ul li:nth-child(2)  ${PRICE2}  css=a[data-sku-priceoriginal="21"]
  Verify Cart    ${TOTPRICE1}
  Change Quantity  2
  Wait until page contains element    css=input[value="2"]
  Element should contain    css=div#total-price-discount.num    ${TOTPRICE2}
  Change Quantity    1
  Wait until page contains    ${TOTPRICE1}    10
  Empty Cart
  Wait until page contains    Ваши покупки будут здесь

Logout from account
  Click Element    css=.auth-area a.logout-link

Ordering
  [Documentation]    Unregistered user tries to order the goods
  Search Of Goods    ariel
  Adding To The Cart  css=.popup-search-results>ul li:nth-child(2)  ${PRICE3}  css=a[data-sku-priceoriginal="206"]
  Wait Until Page Contains Element    table-cart
  Click Element    process_order
  Element Should Contain  css=a.variant.active  Я новый покупатель

Sending empty form
  [Documentation]    Unregistered user tries to send empty form for registration
  Click Element    registration_form_save
  Page Should Contain Element    css=div.order-userinfo

Adding user with existing data
  Fill In The Form  qw23%$..{}  Adams  4  5  1988  0978654321  testemail@gmail.com
  Click Element    registration_form_isSubscriptionRenouncement
  Click Element    registration_form_save
  Element Should Contain    css=div.state-message    Это значение уже используется.

*** Keywords ***
Search Of Goods
  [Arguments]    ${Text}
  Wait Until Page Contains Element    search-q
  Input Text    search-q    ${Text}
  Click Element    search-q
  Wait Until Page Contains Element    css=.popup-search-results>ul li:first-child a

Adding To The Cart
  [Arguments]    ${Locator1}    ${Price}    ${Locator2}
  Click Element  ${Locator1}
  Wait Until Page Contains Element    product-image
  Page Should Contain Element    css=div.price    ${Price}
  Page Should Contain Element    css=div.product-desc
  Click Link    ${Locator2}

Verify Cart
  [Arguments]    ${Price}
  Wait Until Page Contains Element    table-cart
  Element Should Contain    css=div#total-price-discount.num    ${Price}

Change Quantity
  [Arguments]    ${Quantity}
  Execute Javascript    window.$("table#table-cart tr:nth-child(2) input.item-quantity").attr("value","")
  Input Text    css=table#table-cart tr:nth-child(2) input.item-quantity    ${Quantity}
  Click Element    css=div#total-price-discount.num

Empty Cart
  Click Element    css=i.remove-action.js-remove-action
  Wait Until Page Contains    ${PRICE2}
  Execute Javascript    window.$("i.remove-action.js-remove-action").click()

Fill in the form
  [Arguments]    ${Text}  ${Text1}  ${Text2}  ${Text3}  ${Text4}  ${Text5}  ${Text6}
  Input Text    registration_form_firstname    ${Text}
  Input Text    registration_form_lastname    ${Text1}
  Select From List By Index    registration_form_dateOfBirth_day    ${Text2}
  Select From List By Value    registration_form_dateOfBirth_month    ${Text3}
  Select From List By Value    registration_form_dateOfBirth_year    ${Text4}
  Input Text    registration_form_phone    ${Text5}
  Input Text    registration_form_email    ${Text6}
  Click Element    registration_form_isSubscriptionRenouncement
  Click Element    registration_form_save