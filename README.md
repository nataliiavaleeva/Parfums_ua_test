# RobotFramework tests

Test cases cover [parfums.ua](https://parfums.ua/) site
 loginto_parfums_ua.robot - test case for registered user. User should log in to the system and confirm that all is fine.
 headerfooter.robot - there is test suite for not registered user who needs to check all links on header and footer.
 
Robot Framework test cases are executed from the command line by command 'robot'.
