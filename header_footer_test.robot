*** Settings ***
Resource  _resource.robot
Suite Setup  Launch Browser
Suite Teardown  Close All Browsers

*** Variables ***

*** Test Cases ***
Check headers and footers
  [Documentation]  Not logged user clicks on header and footer links
  Check link from header  css=.sec-nav a[href="/static/about"]  css=.text>h1  О магазине
  Check link from header  css=.sec-nav a[href="/static/contacts"]  css=.text>h1  Контакты
  Check link from header  css=.sec-nav a[href="/static/discount-system"]  css=.text>h1  Дисконтная программа
  Check link from header  css=.sec-nav a[href="/static/delivery"]  css=.text>h1  Доставка и оплата
  Check blog link  css=.sec-nav a[href="/blog"]  css=a.special-link
  Click image  css=.logo img
  Check link from header  css=.footer-nav a[href="/static/contacts"]  css=.text>h1  Контакты

Check social network links
  [Documentation]  Not logged user opens social network links
  Check Vkontakte link  css=i.icon-vkontakte  css=h2.page_name  PARFUMS.UA™
  Go to  url=https://parfums.ua/
  Check Facebook link  css=i.icon-facebook  url=https://www.facebook.com/PARFUMS.UA  facebook
  Close window
  Select window  url=https://parfums.ua/
  Check other social network link  css=i.icon-inst  css=h1._i572c.notranslate  parfumsua
  Check other social network link  css=i.icon-google  css=.xRbTYb  PARFUMS.UA's posts

*** Keywords ***

