*** Settings ***
Resource    _resource.robot
Suite Setup    Launch Browser
Suite Teardown    Close All Browsers

*** Variables ***
${Price1}    105
${Price2}    200
${Price3}    763
${Price4}    260

*** Test Cases ***
User logs in to the shop and uses global search
  Click Element    css=.choose-region.show
  Login To The Shop    380976165637    380976165637
  Search Of Goods    shampoo
  Verify Product Page  css=.popup-search-results>ul li:first-child a  ${Price1}
  Search Of Goods    shampoo
  Verify Product Page  css=.popup-search-results>ul li:nth-child(2)  ${Price2}

User are looking for products by category
  Click Element    css=.main-nav>ul li:first-child a
  Click Element    css=.brand-list.extended a[href="/category/parfums/brand=donna_karan"]
  Wait Until Page Contains Element    css=span.reset-filters
  Verify Product Page  css=.product-desc a[href="/product/donna_karan_dkny_be_delicious"]  ${Price3}
  Click Element    css=.main-nav>ul a[href="/category/cosmetics-for-nails"]
  Click Element    css=div.brand-list a[href="/category/cosmetics-for-nails/vid_produkcii=lak_dlya_nogtej"]
  Wait Until Page Contains Element    css=span.reset-filters
  Click Element    css=.product-desc a[href="/product/lak-dlya-nogtey-zoya-big-5-free"]
  Verify Product Page  css=.product-desc a[href="/product/lak-dlya-nogtey-zoya-big-5-free"]  ${Price4}

*** Keywords ***
Search Of Goods
  [Arguments]    ${Text}
  Wait Until Page Contains Element    search-q
  Input Text    search-q    ${Text}
  Click Element    search-q
  Wait Until Page Contains Element    css=.popup-search-results>ul li:first-child a

Verify Product Page
  [Arguments]  ${Locator}  ${Price}
  Click Element  ${Locator}
  Wait Until Page Contains Element  product-image
  Page Should Contain Element  css=div.price    ${Price}